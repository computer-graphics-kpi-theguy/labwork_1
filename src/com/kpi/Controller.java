package com.kpi;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;

import static javafx.scene.paint.Color.BLACK;

public class Controller {

    @FXML
    private Pane mainPane;
    @FXML
    private Pane mainPane1;
    @FXML
    private CheckBox degreeCheckbox;
    @FXML
    private TextField centerXfield;
    @FXML
    private TextField centerYfield;
    @FXML
    private TextField circleColorField;
    @FXML
    private TextField circleRadiusField;
    @FXML
    private TextField degreeField;
    @FXML
    private TextField centerXfield1;
    @FXML
    private TextField centerYfield1;
    @FXML
    private TextField circleRadiusField1;


    @FXML
    public void MouseOrnamentClick(ActionEvent actionEvent){
        double localCenterX = Double.parseDouble(centerXfield1.getCharacters().toString());
        double localCenterY = Double.parseDouble(centerYfield1.getCharacters().toString());
        double localCircleRadius = Double.parseDouble(circleRadiusField1.getCharacters().toString());
        double localDegree = Double.parseDouble(degreeField.getCharacters().toString());
        Ornament myOrnament = new Ornament(localCenterX, localCenterY, localCircleRadius);
        if(degreeCheckbox.isSelected()){
            if(localDegree == 0.0){
                Group localMyOrnament = myOrnament.generateOrnament();
                localMyOrnament.getTransforms().add(new Rotate(localDegree));
                mainPane1.getChildren().add(localMyOrnament);
            }
            else
            for (int i = 0; i < localDegree; i = i + 5) {
                Group localMyOrnament = myOrnament.generateOrnament();
                localMyOrnament.getTransforms().add(new Rotate(i));
                mainPane1.getChildren().addAll(localMyOrnament);
            }
        }
        else {
            Group localMyOrnament = myOrnament.generateOrnament();
            localMyOrnament.getTransforms().add(new Rotate(localDegree));
            mainPane1.getChildren().add(localMyOrnament);
        }
    }

    @FXML
    public void MainPrimitiveClick(ActionEvent actionEvent) {
        double localCenterX = Double.parseDouble(centerXfield.getCharacters().toString());
        double localCenterY = Double.parseDouble(centerYfield.getCharacters().toString());
        double localCircleRadius = Double.parseDouble(circleRadiusField.getCharacters().toString());
        Color localColor = Color.web(circleColorField.getCharacters().toString());
        Primitive myPrimitive = new Primitive(localCenterX, localCenterY, localCircleRadius);
        myPrimitive.setCircleColor(localColor);
        myPrimitive.setCircleStroke(BLACK);
        myPrimitive.setLineStoke(BLACK);
        mainPane.getChildren().add(myPrimitive.makePrimitive());
    }

    public void ClearClick(ActionEvent actionEvent) {
        mainPane.getChildren().clear();
    }

    public void ClearClick2(ActionEvent actionEvent) {
        mainPane1.getChildren().clear();
    }
}
