package com.kpi;

import javafx.scene.Group;


public class Ornament {
    private double centerX;
    private double centerY;
    private double ornamentRadius;

    public Ornament(double centerX, double centerY, double ornamentRadius) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.ornamentRadius = ornamentRadius;
    }

    public Group generateOrnament(){
        Group ornamentGroup = new Group();
        ornamentGroup.setLayoutX(centerX);
        ornamentGroup.setLayoutY(centerY);
        double localRadius = ornamentRadius/5;
        double localRelavity = localRadius*Math.sqrt(2);
        Group[] primitiveGroup = new Group[10];
        primitiveGroup[0] = new Primitive(0.0, 0.0, localRadius).makePrimitive();
        primitiveGroup[1] = new Primitive(-localRelavity, -localRelavity, localRadius).makePrimitive();
        primitiveGroup[2] = new Primitive(-2*localRelavity, -2*localRelavity, localRadius).makePrimitive();
        primitiveGroup[3] = new Primitive(-localRelavity, localRelavity, localRadius).makePrimitive();
        primitiveGroup[4] = new Primitive(-2*localRelavity, 2*localRelavity, localRadius).makePrimitive();
        primitiveGroup[5] = new Primitive(localRelavity, -localRelavity, localRadius).makePrimitive();
        primitiveGroup[6] = new Primitive(2*localRelavity, -2*localRelavity, localRadius).makePrimitive();
        primitiveGroup[7] = new Primitive(localRelavity, localRelavity, localRadius).makePrimitive();
        primitiveGroup[8] = new Primitive(2*localRelavity, 2*localRelavity, localRadius).makePrimitive();
        primitiveGroup[9] = new Primitive(0.0, 0.0, ornamentRadius).makePrimitive();
        ornamentGroup.getChildren().addAll(primitiveGroup);
        return ornamentGroup;
    }
}
