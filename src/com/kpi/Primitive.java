package com.kpi;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.transform.Rotate;


public class Primitive {
    private double centerX;
    private double centerY;
    private double circleRadius;
    private Color LineStoke = Color.BLACK;
    private Color CircleStroke = Color.BLACK;
    private Color CircleColor = Color.TRANSPARENT;

    public void setCircleColor(Color circleColor) {
        CircleColor = circleColor;
    }

    public void setLineStoke(Color lineStoke) {
        LineStoke = lineStoke;
    }

    public void setCircleStroke(Color circleStroke) {
        CircleStroke = circleStroke;
    }

    public Primitive(double centerX,double centerY, double circleRadius) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.circleRadius = circleRadius;
    }

    public Group makePrimitive() {
        Group primitiveGroup = new Group();
        primitiveGroup.setLayoutX(centerX);
        primitiveGroup.setLayoutY(centerY);
        Circle mainCircle = new Circle(0.0, 0.0, circleRadius);
        Line lineX = new Line();
        Line lineY = new Line();
        primitiveGroup.getChildren().addAll(mainCircle, lineX, lineY);
        mainCircle.setFill(CircleColor);
        mainCircle.setStroke(CircleStroke);
        lineX.setStartX(-circleRadius);
        lineX.setStartY(0.0);
        lineX.setEndX(circleRadius);
        lineX.setEndY(0.0);
        lineX.setStroke(LineStoke);
        lineX.getTransforms().add(new Rotate(45, 0 ,0));
        lineY.setStartX(0.0);
        lineY.setStartY(-circleRadius);
        lineY.setEndX(0.0);
        lineY.setEndY(circleRadius);
        lineY.setStroke(LineStoke);
        lineY.getTransforms().add(new Rotate(45, 0, 0));
        return primitiveGroup;
    }
}
